using UnityEngine;
using VRStandardAssets.Utils;

namespace VRStandardAssets.Examples
{
    // This script is a simple example of how an interactive item can
    // be used to change things on gameobjects by handling events.
    public class ExampleInteractiveItem : MonoBehaviour
    {
        [SerializeField] private Material m_NormalMaterial;                
        [SerializeField] private Material m_OverMaterial;                  
        [SerializeField] private Material m_ClickedMaterial;               
        [SerializeField] private Material m_DoubleClickedMaterial;         
        [SerializeField] private VRInteractiveItem m_InteractiveItem;
		[SerializeField] private Renderer[] renderers;
//		[SerializeField] private Renderer m2_Renderer;
//		[SerializeField] private Renderer m2_Renderer;
//		[SerializeField] private Renderer m3_Renderer;
//		[SerializeField] private Renderer m4_Renderer;
//		[SerializeField] private Renderer m5_Renderer;
//		[SerializeField] private Renderer m6_Renderer;
//		[SerializeField] private Renderer m7_Renderer;
//		[SerializeField] private Renderer m8_Renderer;
//		[SerializeField] private Renderer m9_Renderer;
//		[SerializeField] private Renderer m10_Renderer;
//		[SerializeField] private Renderer m11_Renderer;
//		[SerializeField] private Renderer m12_Renderer;
//		[SerializeField] private Renderer m13_Renderer;
//		[SerializeField] private Renderer m14_Renderer;
//		[SerializeField] private Renderer m15_Renderer;
//		[SerializeField] private Renderer m16_Renderer;
//		[SerializeField] private Renderer m17_Renderer;
//		[SerializeField] private Renderer m18_Renderer;
//		[SerializeField] private Renderer m19_Renderer;
//		[SerializeField] private Renderer m20_Renderer;
//		[SerializeField] private Renderer m21_Renderer;
//		[SerializeField] private Renderer m22_Renderer;
//		[SerializeField] private Renderer m23_Renderer;
//		[SerializeField] private Renderer m24_Renderer;
//		[SerializeField] private Renderer m25_Renderer;
//		[SerializeField] private Renderer m26_Renderer;
//		[SerializeField] private Renderer m27_Renderer;
//		[SerializeField] private Renderer m28_Renderer;
//		[SerializeField] private Renderer m29_Renderer;
//		[SerializeField] private Renderer m30_Renderer;
//		[SerializeField] private Renderer m31_Renderer;
//		[SerializeField] private Renderer m32_Renderer;
	//	[SerializeField] private Renderer m33_Renderer;
	

        private void Awake ()
		{
			for (int i = 0; i < renderers.Length; i++) {
				renderers [i].material = m_NormalMaterial;
			}
		}
			
				
			
//			m2_Renderer.material = m_NormalMaterial;
//			m3_Renderer.material = m_NormalMaterial;
//			m4_Renderer.material = m_NormalMaterial;
//			m5_Renderer.material = m_NormalMaterial;
//			m6_Renderer.material = m_NormalMaterial;
//			m7_Renderer.material = m_NormalMaterial;
//			m8_Renderer.material = m_NormalMaterial;
//			m9_Renderer.material = m_NormalMaterial;
//			m10_Renderer.material = m_NormalMaterial;
//			m11_Renderer.material = m_NormalMaterial;
//			m12_Renderer.material = m_NormalMaterial;
//			m13_Renderer.material = m_NormalMaterial;
//			m14_Renderer.material = m_NormalMaterial;
//			m15_Renderer.material = m_NormalMaterial;
//			m16_Renderer.material = m_NormalMaterial;
//			m17_Renderer.material = m_NormalMaterial;
//			m18_Renderer.material = m_NormalMaterial;
//			m19_Renderer.material = m_NormalMaterial;
//			m20_Renderer.material = m_NormalMaterial;
//			m21_Renderer.material = m_NormalMaterial;
//			m22_Renderer.material = m_NormalMaterial;
//			m23_Renderer.material = m_NormalMaterial;
//			m24_Renderer.material = m_NormalMaterial;
//			m25_Renderer.material = m_NormalMaterial;
//			m26_Renderer.material = m_NormalMaterial;
//			m27_Renderer.material = m_NormalMaterial;
//			m28_Renderer.material = m_NormalMaterial;
//			m29_Renderer.material = m_NormalMaterial;
//			m30_Renderer.material = m_NormalMaterial;
//			m31_Renderer.material = m_NormalMaterial;
//			m32_Renderer.material = m_NormalMaterial;
//		//	m33_Renderer.material = m_NormalMaterial;

		


        private void OnEnable()
        {
            m_InteractiveItem.OnOver += HandleOver;
            m_InteractiveItem.OnOut += HandleOut;
            m_InteractiveItem.OnClick += HandleClick;
            m_InteractiveItem.OnDoubleClick += HandleDoubleClick;
        }


        private void OnDisable()
        {
            m_InteractiveItem.OnOver -= HandleOver;
            m_InteractiveItem.OnOut -= HandleOut;
            m_InteractiveItem.OnClick -= HandleClick;
            m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;
        }


        //Handle the Over event
		private void HandleOver()
        {
          
				for(int i = 0; i < renderers.Length; i++){
						renderers[i].material = m_OverMaterial;
				Debug.Log("Show over state");
			}
									}

//				m_Renderer.material = m_OverMaterial;
//			m2_Renderer.material = m_OverMaterial;
//			m3_Renderer.material = m_OverMaterial;
//			m4_Renderer.material = m_OverMaterial;
//			m5_Renderer.material = m_OverMaterial;
//
//
//			m6_Renderer.material = m_OverMaterial;
//			m7_Renderer.material = m_OverMaterial;
//			m8_Renderer.material = m_OverMaterial;
//			m9_Renderer.material = m_OverMaterial;
//			m10_Renderer.material = m_OverMaterial;
//			m11_Renderer.material = m_OverMaterial;
//			m12_Renderer.material = m_OverMaterial;
//			m13_Renderer.material = m_OverMaterial;
//			m14_Renderer.material = m_OverMaterial;
//			m15_Renderer.material = m_OverMaterial;
//
//			m16_Renderer.material = m_OverMaterial;
//			m17_Renderer.material = m_OverMaterial;
//			m18_Renderer.material = m_OverMaterial;
//			m19_Renderer.material = m_OverMaterial;
//			m20_Renderer.material = m_OverMaterial;
//			m21_Renderer.material = m_OverMaterial;
//			m22_Renderer.material = m_OverMaterial;
//			m23_Renderer.material = m_OverMaterial;
//			m24_Renderer.material = m_OverMaterial;
//			m25_Renderer.material = m_OverMaterial;
//
//			m26_Renderer.material = m_OverMaterial;
//			m27_Renderer.material = m_OverMaterial;
//			m28_Renderer.material = m_OverMaterial;
//			m29_Renderer.material = m_OverMaterial;
//			m30_Renderer.material = m_OverMaterial;
//			m31_Renderer.material = m_OverMaterial;
//			m32_Renderer.material = m_OverMaterial;
//		//	m33_Renderer.material = m_OverMaterial;
//        }


        //Handle the Out event
        private void HandleOut()
		{
			Debug.Log ("Show out state");


			for (int i = 0; i < renderers.Length; i++) {
				renderers [i].material = m_NormalMaterial;
				Debug.Log ("Show normal state");
			}
		}
//			m_Renderer.material = m_NormalMaterial;
//			m2_Renderer.material = m_NormalMaterial;
//			m3_Renderer.material = m_NormalMaterial;
//			m4_Renderer.material = m_NormalMaterial;
//			m5_Renderer.material = m_NormalMaterial;
//
//
//			m6_Renderer.material = m_NormalMaterial;
//			m7_Renderer.material = m_NormalMaterial;
//			m8_Renderer.material = m_NormalMaterial;
//			m9_Renderer.material = m_NormalMaterial;
//			m10_Renderer.material = m_NormalMaterial;
//			m11_Renderer.material = m_NormalMaterial;
//			m12_Renderer.material = m_NormalMaterial;
//			m13_Renderer.material = m_NormalMaterial;
//			m14_Renderer.material = m_NormalMaterial;
//			m15_Renderer.material = m_NormalMaterial;
//
//			m16_Renderer.material = m_NormalMaterial;
//			m17_Renderer.material = m_NormalMaterial;
//			m18_Renderer.material = m_NormalMaterial;
//			m19_Renderer.material = m_NormalMaterial;
//			m20_Renderer.material = m_NormalMaterial;
//			m21_Renderer.material = m_NormalMaterial;
//			m22_Renderer.material = m_NormalMaterial;
//			m23_Renderer.material = m_NormalMaterial;
//			m24_Renderer.material = m_NormalMaterial;
//			m25_Renderer.material = m_NormalMaterial;
//
//			m26_Renderer.material = m_NormalMaterial;
//			m27_Renderer.material = m_NormalMaterial;
//			m28_Renderer.material = m_NormalMaterial;
//			m29_Renderer.material = m_NormalMaterial;
//			m30_Renderer.material = m_NormalMaterial;
//			m31_Renderer.material = m_NormalMaterial;
//			m32_Renderer.material = m_NormalMaterial;
//			//m33_Renderer.material = m_NormalMaterial;
//
        


        //Handle the Click event
        private void HandleClick()
        {
            Debug.Log("Show click state");
//            m_Renderer.material = m_ClickedMaterial;
//			m2_Renderer.material = m_ClickedMaterial;
//			m3_Renderer.material = m_ClickedMaterial;
//			m4_Renderer.material = m_ClickedMaterial;
//			m3_Renderer.material = m_ClickedMaterial;
        }


        //Handle the DoubleClick event
        private void HandleDoubleClick()
        {
            Debug.Log("Show double click");
           // m_Renderer.material = m_DoubleClickedMaterial;
        }
    }
}